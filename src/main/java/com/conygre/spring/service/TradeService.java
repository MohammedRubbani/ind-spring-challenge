package com.conygre.spring.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.data.TradeRepository;
import com.conygre.spring.entities.trade.Trade;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TradeService {
  
  @Autowired
  private TradeRepository dao;

  public void addToCatalog(Trade trade){
    dao.insert(trade);

  }

  public Collection<Trade> getTrades(){
    return dao.findAll();
  }

  public Optional<Trade> getTradeById(ObjectId id){
    return dao.findById(id);
  }

  public Collection<Trade> getTradeByTicker(String ticker){
    return dao.findByTicker(ticker);
  }

  public Collection<Trade> getTradeByDate(String date){
    return dao.findByDate(date);
  }

  public Collection<Trade> getTradeByType(String type){
    return dao.findByType(type);
  }
  
  public void deleteTrades(){
    dao.deleteAll();
  }

  public void deleteTradeById(ObjectId id) {
    dao.deleteById(id);
  }
  

}