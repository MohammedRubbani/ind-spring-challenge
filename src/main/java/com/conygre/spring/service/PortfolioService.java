package com.conygre.spring.service;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.data.PortfolioRepository;
import com.conygre.spring.entities.portfolio.Portfolio;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PortfolioService {
  
  @Autowired
  private PortfolioRepository dao;

  public void addToCatalog(Portfolio Portfolio){
    //Portfolio.setValue(Portfolio.getPrice() * Portfolio.getQuantity());
    dao.insert(Portfolio);
  }

  public Collection<Portfolio> getPortfolios(){
    return dao.findAll();
  }

  public Optional<Portfolio> getPortfolioById(ObjectId id){
    return dao.findById(id);
  }

  public Collection<Portfolio> getPortfolioByTicker(String ticker){
    return dao.findByTicker(ticker);
  }

  public Collection<Portfolio> getPortfolioByDate(String date){
    return dao.findByDate(date);
  }

  public Collection<Portfolio> getPortfolioByQuantity(String quantity){
    //String stringQuant = Double.toString(string);
    return dao.findByQuantity(quantity);
  }

  
  public Collection<Portfolio> getPortfolioByPrice(String price){
  // String stringPrice = Double.toString(price);
    return dao.findByPrice(price);
  }
  
  public void deletePortfolioById(ObjectId id) {
    dao.deleteById(id);
}

}