package com.conygre.spring.data;

import java.util.List;

import com.conygre.spring.entities.portfolio.Portfolio;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*")
public interface PortfolioRepository extends MongoRepository<Portfolio, ObjectId> {
  
  public List<Portfolio> findByTicker(String ticker);

  public List<Portfolio> findByDate(String date);

  public List<Portfolio> findByQuantity(String quantity);

  public List<Portfolio> findByPrice(String price);


}