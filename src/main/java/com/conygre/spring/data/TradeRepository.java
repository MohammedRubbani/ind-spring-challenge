package com.conygre.spring.data;

import java.util.List;

import com.conygre.spring.entities.trade.Trade;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "*")
public interface TradeRepository extends MongoRepository<Trade, ObjectId> {
  
  public List<Trade> findByTicker(String ticker);

  public List<Trade> findByDate(String date);

  public List<Trade> findByType(String type);


}