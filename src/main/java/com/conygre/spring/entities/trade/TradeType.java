package com.conygre.spring.entities.trade;

public enum TradeType {
  BUY("BUY"),
  SELL("SELL");

  private String type;

  private TradeType(String type) {
    this.type= type;
  }

public String getState() {
    return this.type;
  }

}
