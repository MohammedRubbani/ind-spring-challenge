package com.conygre.spring.entities.trade;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Trade {

  @Id
  private String id;
  //Need to change date to a Datetype
  private String date;
  private String ticker;
  private int quantity;
  private double price;
  private TradeType type;
  private TradeState state;


  public Trade(){};



  public Trade(String date, String ticker, int quantity, double price, TradeType type, TradeState state){
    this.date = date;
    this.ticker = ticker;
    this.quantity = quantity;
    this.price = price;
    this.type = type;
    this.state = state;

  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public String getTicker() {
    return ticker;
  }

  public void setTicker(String ticker) {
    this.ticker = ticker;
  }

  public int getQuantity() {
    return quantity;
  }

  public void setQuantity(int quantity) {
    this.quantity = quantity;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }


  public TradeType getType() {
    return type;
  }

  public void setType(TradeType type) {
    this.type = type;
  }


  public TradeState getState() {
    return state;
  }

  public void setState(TradeState state) {
    this.state = state;
  }



  
}