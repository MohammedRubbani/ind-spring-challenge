package com.conygre.spring.entities.portfolio;

public enum PortfolioType {
    STOCKS("Stocks"),
    CASH("Cash"),
    BONDS("Bonds");

    private String portfoliotype;

    private PortfolioType(String portfoliotype) {
        this.portfoliotype = portfoliotype;
    }
    public String getPortfolioType() {
        return this.portfoliotype;
    }
}
