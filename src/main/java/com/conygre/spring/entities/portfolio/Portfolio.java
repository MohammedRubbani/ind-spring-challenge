package com.conygre.spring.entities.portfolio;

import com.conygre.spring.entities.trade.Trade;
import com.conygre.spring.entities.trade.TradeState;
import com.conygre.spring.entities.trade.TradeType;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Portfolio extends Trade {

  
  private double lastClosePrice;

  
  public Portfolio(){};

  public Portfolio(String date, String ticker, int quantity, double price,
                  TradeType type, TradeState state) {

    super(date, ticker, quantity, price, type, state);    
    this.lastClosePrice = 0;

  }
  
  public double getLastClosePrice() {
    return lastClosePrice;
  }

  public void setLastClosePrice(double price) {
    this.lastClosePrice = price + (Math.random()*10); 
  
  }

 
}