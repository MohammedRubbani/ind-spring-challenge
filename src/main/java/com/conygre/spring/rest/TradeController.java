package com.conygre.spring.rest;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.entities.trade.Trade;
import com.conygre.spring.service.TradeService;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/trades/")
public class TradeController {
  
  private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

  
  @Autowired
  private TradeService tradeService;

  

  // RETRIEVE
  @RequestMapping(method=RequestMethod.GET)
  public Collection<Trade> getTrades() {
    LOG.debug("getAllTrades() called");
    return tradeService.getTrades();
  }

  // RETRIEVE
  @RequestMapping(method=RequestMethod.GET, value = "trade_id:{id}")
  public Optional<Trade> getTradeById(@PathVariable("id") String id) {
    return tradeService.getTradeById(new ObjectId(id));
  }

  // RETRIEVE
  @RequestMapping(method=RequestMethod.GET, value = "{type}")
  public Collection<Trade> getTradeByType(@PathVariable("type") String type) {
    return tradeService.getTradeByType(new String(type));
  } 

  // RETRIEVE
  @RequestMapping(method=RequestMethod.GET, value = "trade_ticker:{ticker}")
  public Collection<Trade> getTradeByTicker(@PathVariable("ticker") String ticker) {
    return tradeService.getTradeByTicker(new String(ticker));
  }

  // RETRIEVE
  @RequestMapping(method=RequestMethod.GET, value = "trade_date:{date}")
  public Collection<Trade> getTradeByDate(@PathVariable("date") String date) {
    return tradeService.getTradeByDate(new String(date));
  }


  // CREATE
  @RequestMapping(method=RequestMethod.POST)
  public void addTrade(@RequestBody Trade t) {
    LOG.debug("addTrade() called : " + t);
    tradeService.addToCatalog(t);
  }

    //DELETE
	@RequestMapping(method = RequestMethod.DELETE, value = "delete/")
	public void deleteTrades() {
		tradeService.deleteTrades();
  }
  
    //DELETE
	@RequestMapping(method = RequestMethod.DELETE, value = "delete/{id}")
	public void deleteCd(@PathVariable("id") String id) {
		tradeService.deleteTradeById(new ObjectId(""+id));
	}
  
}