package com.conygre.spring.rest;

import java.util.Collection;
import java.util.Optional;

import com.conygre.spring.entities.portfolio.Portfolio;
import com.conygre.spring.service.PortfolioService;

import org.bson.types.ObjectId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/portfolios/")
public class PortfolioController {

  private static final Logger LOG = LoggerFactory.getLogger(PortfolioController.class);

  @Autowired
  private PortfolioService portfolioService;

  // RETRIEVE
  @RequestMapping(method = RequestMethod.GET)
  public Collection<Portfolio> getPortfolios() {
    LOG.debug("getAllPortfolios() called");
    return portfolioService.getPortfolios();
  }

  // RETRIEVE
  @RequestMapping(method = RequestMethod.GET, value = "portfolio_id:{id}")
  public Optional<Portfolio> getPortfolioById(@PathVariable("id") String id) {
    return portfolioService.getPortfolioById(new ObjectId(id));
  }

  // RETRIEVE
  @RequestMapping(method = RequestMethod.GET, value = "portfolio_ticker:{ticker}")
  public Collection<Portfolio> getPortfolioByTicker(@PathVariable("ticker") String ticker) {
    return portfolioService.getPortfolioByTicker(new String(ticker));
  }

  // RETRIEVE
  @RequestMapping(method = RequestMethod.GET, value = "portfolio_date:{date}")
  public Collection<Portfolio> getPortfolioByDate(@PathVariable("date") String date) {
    return portfolioService.getPortfolioByDate(new String(date));
  }

  // RETRIEVE
  @RequestMapping(method = RequestMethod.GET, value = "portfolio_quantity:{quantity}")
  public Collection<Portfolio> getPortfolioByQuantity(@PathVariable("quantity") Double quantity) {

    String stringQuant=quantity.toString();
    return portfolioService.getPortfolioByQuantity(new String(stringQuant));
    }
  
    // RETRIEVE
    @RequestMapping(method=RequestMethod.GET, value = "portfolio_date:{price}")
    public Collection<Portfolio> getPortfolioBPrice(@PathVariable("price") Double price) {
      String stringPrice=price.toString();

      return portfolioService.getPortfolioByPrice(new String(stringPrice));
    }

  // CREATE
  @RequestMapping(method=RequestMethod.POST)
  public void addPortfolio(@RequestBody Portfolio t) {
    LOG.debug("addPortfolio() called : " + t);
    portfolioService.addToCatalog(t);
  }

  //UPDATE
  @RequestMapping(method=RequestMethod.PUT, value = "portfolio_date:{ticker}")
  public void updatePortfolioById(@PathVariable("ticker") String ticker) {
    portfolioService.getPortfolioByTicker(new String(ticker));
  }

  

  //DELETE
	@RequestMapping(method = RequestMethod.DELETE, value = "delete/{id}")
	public void deleteCd(@PathVariable("id") String id) {
		portfolioService.deletePortfolioById(new ObjectId(""+id));
	}
  

}