package com.conygre.spring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.conygre.TestMongoConfig;
import com.conygre.spring.entities.portfolio.Portfolio;
import com.conygre.spring.entities.trade.TradeState;
import com.conygre.spring.entities.trade.TradeType;
import com.conygre.spring.service.PortfolioService;
import com.mongodb.BasicDBObject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestMongoConfig.class, 
loader = AnnotationConfigContextLoader.class)
@WebAppConfiguration
public class PortfolioServiceLayerTests {
  
  @Autowired
  private MongoTemplate mongoTemplate;
  
  @Autowired
  private PortfolioService service;

  @BeforeEach
  public void setup() {


    Portfolio Portfolio1 = new Portfolio("11-09-2020", "AAPL",10,100,TradeType.BUY,TradeState.CREATED);
    Portfolio Portfolio2 = new Portfolio("13-09-2020", "TSLA",10,100,TradeType.BUY,TradeState.CREATED);
    Portfolio Portfolio3 = new Portfolio("14-09-2020", "AAPL",10,100,TradeType.SELL,TradeState.CREATED);
    Portfolio Portfolio4 = new Portfolio("16-09-2020", "ZM",10,100,TradeType.BUY,TradeState.CREATED);
    Portfolio Portfolio5 = new Portfolio("16-09-2020", "AAPL",10,100,TradeType.SELL,TradeState.CREATED);



    service.addToCatalog(Portfolio1);
    service.addToCatalog(Portfolio2);
    service.addToCatalog(Portfolio3);
    service.addToCatalog(Portfolio4);
    service.addToCatalog(Portfolio5);

  }

  @Test
  public void GetAllPortfoliosThroughServiceLayer() {
    assertEquals(5, service.getPortfolios().size());
  }

  @Test 
  public void findByTickerThroughServiceLayer() {

    assertEquals(3, service.getPortfolioByTicker("AAPL").size());
  }

  @Test 
  public void findByDateThroughServiceLayer() {

    assertEquals(2, service.getPortfolioByDate("16-09-2020").size());
  }


  @AfterEach
  public void cleanUp() {
    for (String collectionName : mongoTemplate.getCollectionNames()) {
          if (!collectionName.startsWith("system.")) {
              mongoTemplate.getCollection(collectionName).deleteMany(new BasicDBObject());
              
          }
      }
    }
}
