package com.conygre.spring;

import static org.junit.jupiter.api.Assertions.assertEquals;

import com.conygre.TestMongoConfig;
import com.conygre.spring.entities.trade.Trade;
import com.conygre.spring.entities.trade.TradeState;
import com.conygre.spring.entities.trade.TradeType;
import com.conygre.spring.service.TradeService;
import com.mongodb.BasicDBObject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.test.context.web.WebAppConfiguration;


@ExtendWith(SpringExtension.class)
@ContextConfiguration(classes = TestMongoConfig.class, 
loader = AnnotationConfigContextLoader.class)
@WebAppConfiguration
public class TradeServiceLayerTests {
  
  @Autowired
  private MongoTemplate mongoTemplate;
  
  @Autowired
  private TradeService service;

  @BeforeEach
  public void setup() {

    Trade trade1 = new Trade("11-09-2020", "AAPL", 10, 100, TradeType.BUY, TradeState.CREATED);
    Trade trade2 = new Trade("10-09-2020", "ZM", 10, 100, TradeType.BUY, TradeState.CREATED);
    Trade trade3 = new Trade("01-09-2020", "TSLA", 10, 100, TradeType.BUY, TradeState.CREATED);
    Trade trade4 = new Trade("06-09-2020", "AAPL", 10, 100, TradeType.SELL,TradeState.CREATED);
    Trade trade5 = new Trade("06-09-2020", "ZM", 10, 100,TradeType.BUY, TradeState.CREATED);

    service.addToCatalog(trade1);
    service.addToCatalog(trade2);
    service.addToCatalog(trade3);
    service.addToCatalog(trade4);
    service.addToCatalog(trade5);

  }

  @Test
  public void GetAllTradesThroughServiceLayer() {
    assertEquals(5, service.getTrades().size());
  }

  @Test 
  public void findByTickerThroughServiceLayer() {

    assertEquals(2, service.getTradeByTicker("AAPL").size());
  }

  @Test 
  public void findByDateThroughServiceLayer() {

    assertEquals(2, service.getTradeByDate("06-09-2020").size());
  }

  @Test
  public void findByTradeTypeThroughServiceLayer() {
    assertEquals(1, service.getTradeByType("SELL").size());
  }


  @AfterEach
  public void cleanUp() {
    for (String collectionName : mongoTemplate.getCollectionNames()) {
          if (!collectionName.startsWith("system.")) {
              mongoTemplate.getCollection(collectionName).deleteMany(new BasicDBObject());
              
          }
      }
    }
}
