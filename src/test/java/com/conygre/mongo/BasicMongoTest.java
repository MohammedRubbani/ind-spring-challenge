package com.conygre.mongo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import com.conygre.TestMongoConfig;
import com.conygre.spring.entities.trade.Trade;
import com.conygre.spring.entities.trade.TradeState;
import com.conygre.spring.entities.trade.TradeType;
import com.mongodb.BasicDBObject;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.AnnotationConfigContextLoader;


@DataMongoTest
@ContextConfiguration(classes = TestMongoConfig.class, 
loader = AnnotationConfigContextLoader.class)
public class BasicMongoTest{


  @Autowired
  private MongoTemplate mongoTemplate;

  @Test
  public void canInsertSuccessfully() {

    Trade trade1 = new Trade("11-09-2020", "AAPL", 10, 100, TradeType.BUY, TradeState.CREATED);
    Trade trade2 = new Trade("11-09-2020", "AAPL", 10, 100, TradeType.BUY, TradeState.CREATED);
    Trade trade3 = new Trade("11-09-2020", "AAPL", 10, 100, TradeType.SELL, TradeState.CREATED);

    mongoTemplate.insert(trade1);
    mongoTemplate.insert(trade2);
    mongoTemplate.insert(trade3);

    List<Trade> trades = mongoTemplate.findAll(Trade.class);
    trades.forEach(trade -> System.out.println(trade.getTicker()));

    assertEquals(3, trades.size());
  }

  @AfterEach
  public void cleanUp() {
    for (String collectionName : mongoTemplate.getCollectionNames()) {
          if (!collectionName.startsWith("system.")) {
              mongoTemplate.getCollection(collectionName).deleteMany(new BasicDBObject());
              
          }
      }
  }

}
