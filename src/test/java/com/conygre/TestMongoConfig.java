package com.conygre;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@ComponentScan
@EnableMongoRepositories(basePackages = "com.conygre.spring.data")
public class TestMongoConfig extends AbstractMongoClientConfiguration {

  @Override
  protected String getDatabaseName() {
    // TODO Auto-generated method stub
    return "tradedb1";
  }

}
